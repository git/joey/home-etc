PATH=$HOME/bin:$PATH:/usr/sbin:/sbin

# If not running interactively, don't do anything else
[ -z "$PS1" ] && return

. $HOME/.environment
eval `dircolors 2>/dev/null`

# don't put duplicate lines in the history
export HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
shopt -s histappend

# append to the history file, don't overwrite it
shopt -s histappend

# store multi-line commands in history
shopt -s cmdhist

# this breaks eg "bts show #nnnnnn"
shopt -u interactive_comments

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Name directory to change to it.
shopt -s autocd

# enable completion
if [ -f /etc/bash_completion ]; then
	. /etc/bash_completion
fi

# aliases
alias df='df -h -x none'
alias free='free -m'
alias c='cd; clear'
alias ls='ls -b -CF --color=auto'
alias l=ls
alias new='ls -b -CF --color=auto -rt'
alias n=new
alias dir='ls -lha'
alias du='du -h'
alias cal='ncal -b -3'
alias f=finger
alias edit=$EDITOR
alias e=$EDITOR
alias ftp=lftp
alias md=mkdir
alias rd=rmdir
alias k=killall
alias mtr='mtr --curses'
alias bug='reportbug -b --no-check-available -x -M --mode=expert'
alias slrn='slrn -n'
alias menu=pdmenu
alias stardate='date "+%y%m.%d/%H%M"'
alias debcommit='commit -a'
alias zxgv=xzgv
alias zxgv=xzgv
alias bc='bc -l -q'
alias pal='pal -c 0'
alias units='units -1v -d max'
# avoid using builtin
alias time='/usr/bin/time'

# Stuff that I just don't want to run, because it sucks.
alias eview='echo "eview? No"'
# Argh, mf is so bloody annoying.
alias mf='echo "Er, if you want mv, type mv; if you want mf, I pity you."'

umask 022

update_title () {
	printf "\e]0;$USER@$HOSTNAME: %s\a" "$1"
}

# Update title before executing a command: set it to the full command
show_command () {
	this_command="`history 1`"
	update_title "${this_command/+([ ])+([0-9])+([ ])/}"
}

# Things to do before displaying the command prompt, including printing
# nonzero exit status of the last run command, and setting the git branch.
prompt_command_notitle () {
	local code="$?"
	if [ "$code" -ne 0 ]; then
		echo "- exit $code"
	fi
	# I am almost always in a git repo, and generally at the top
	# of it, so try less expensive branch finding method first.
	local fullbranch="$(cat .git/HEAD 2>/dev/null)"
	if [ "$fullbranch" = "" ]; then
		local fullbranch="$(git symbolic-ref HEAD 2>/dev/null)"
	fi
	local branch="${fullbranch##*/}"
	if [ "$branch" = master ]; then
		vcsinfo=""
	else
		vcsinfo="${branch:+#$branch}"
	fi
}
prompt_command () {
	prompt_command_notitle
	update_title
}

if [ -r /etc/debian_chroot ]; then
	chroot=$(cat /etc/debian_chroot)
else
	chroot=""
fi

PS1='${chroot:+($chroot)}\u@\h:\w$vcsinfo>'

case "$TERM" in
xterm*|rxvt*|screen*|alacritty)
	PS1="\[\e]0;${chroot:+($chroot)}\u@\h:\w\a\]$PS1"
	trap show_command DEBUG
	PROMPT_COMMAND=prompt_command
	;;
*)
	PROMPT_COMMAND=prompt_command_notitle
	;;
esac

# Disable bash mail check.
unset MAILCHECK
