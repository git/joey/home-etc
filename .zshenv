# Yes, I have my own zsh setup; ignore system one.
IGNORE_SYSTEM_ZSH=1

# Source global environment file.
. $HOME/.environment

# Zsh-specific environment settings follow.

# Even if my editor is vi or vim, I do not want
# that at the command line.
bindkey -e

# Don't allow dups in path. Then throw in what I consider is a complete
# path. This results in any other path setting going to the end.
typeset -U path
path=(~/bin /usr/local/bin /usr/bin /usr/X11R6/bin /bin /usr/sbin /sbin /usr/games /usr/lib/surfraw)

# Set up a history file.
HISTFILE=~/.history
SAVEHIST=1000
HISTSIZE=1000

# Some named directories for ease of use.
hash -d doc=/usr/share/doc

# Yee haw.
#if perl -MCoy -e '' 2>/dev/null; then
#	PERL5OPT=-MCoy
#	export PERL5OPT
#fi

# Work around mutt bug #316388
#GPG_TTY=`tty`
#export GPG_TTY
