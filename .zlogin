if command -v uptime >/dev/null; then
	uptime
fi

# Display battery status on portables.
if [ -e /proc/apm ] && command -v apm >/dev/null; then
	apm
elif command -v acpi >/dev/null; then
	acpi
fi	

# Display system news if there is any. Else, show fortune.
if command -v news >/dev/null && news; then
	:
elif command -v fortune >/dev/null; then
	fortune -a
fi

if command -v mesg >/dev/null; then
	mesg y
fi
