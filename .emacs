(setq inhibit-splash-screen t)
(setq visible-bell t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(blink-cursor-mode nil)
 '(custom-enabled-themes '(manoj-dark))
 '(frame-background-mode 'dark)
 '(lsp-file-watch-ignored
   '("[/\\\\]\\.git$" "[/\\\\]\\.hg$" "[/\\\\]\\.bzr$" "[/\\\\]_darcs$" "[/\\\\]\\.svn$" "[/\\\\]_FOSSIL_$" "[/\\\\]\\.idea$" "[/\\\\]\\.ensime_cache$" "[/\\\\]\\.eunit$" "[/\\\\]node_modules$" "[/\\\\]\\.fslckout$" "[/\\\\]\\.tox$" "[/\\\\]\\.stack-work$" "[/\\\\]\\.bloop$" "[/\\\\]\\.metals$" "[/\\\\]target$" "[/\\\\]\\.ccls-cache$" "[/\\\\]\\.deps$" "[/\\\\]build-aux$" "[/\\\\]autom4te.cache$" "[/\\\\]\\.reference$" "doc" "static" "dist-newstyle"))
 '(lsp-file-watch-threshold 2000)
 '(lsp-haskell-hlint-on nil)
 '(lsp-ui-doc-delay 0.5)
 '(lsp-ui-doc-position 'bottom)
 '(tool-bar-mode nil))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Bitstream Vera Sans Mono" :foundry "Bits" :slant normal :weight normal :height 132 :width normal))))
 '(diff-hl-change ((t (:background "#00ff00"))))
 '(diff-hl-delete ((t (:background "#ff0000"))))
 '(diff-hl-insert ((t (:background "#00ffff")))))

(require 'evil)
(evil-mode t)

;; I want tab to always insert a real tab, no magic indentation
(global-set-key (kbd "TAB") 'self-insert-command)
(setq indent-tabs-mode t)
;; backspace into a tab should delete it all, not convert to spaces
(setq backward-delete-char-untabify-method 'hungry)
;; haskell mode does its own indentation; this plugin disables it
(add-hook 'haskell-mode-hook (lambda () (haskell-tab-indent-mode)))

;; visible tabs and trailing spaces
;;(setq whitespace-style '(face tabs tab-mark trailing))
;;(custom-set-faces
;; '(whitespace-tab ((t (:foreground "#636363")))))
;;(setq whitespace-display-mappings
;;  '((tab-mark 9 [124 9] [92 9]))) ; 124 is the ascii ID for '\|'
;;(global-whitespace-mode) ;

;; On-the-fly diff updates
(diff-hl-flydiff-mode)
;; Enable diff-hl globally
(global-diff-hl-mode 1)

;; lsp-haskell hooks
(add-hook 'haskell-mode-hook #'lsp)
(add-hook 'haskell-literate-mode-hook #'lsp)
