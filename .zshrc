# Aliases and functions {{{1

# Xterm resizing-fu. Note that these are utf-8 fonts
alias hide='echo -en "\033]50;nil2\007"'
alias tiny='echo -en "\033]50;-misc-fixed-medium-r-normal--8-80-75-75-c-50-iso10646-1\007"'
alias small='echo -en "\033]50;6x10\007"'
alias default='echo -e "\033]50;-misc-fixed-medium-r-semicondensed--13-*-*-*-*-*-iso10646-1\007"'
alias medium='echo -en "\033]50;-misc-fixed-medium-r-normal--13-120-75-75-c-80-iso10646-1\007"'
alias large='echo -en "\033]50;-misc-fixed-medium-*-*-*-15-*-*-*-*-*-iso10646-1\007"'
# This is a large font that has a corresponding double-width font for 
# CJK and other characters, useful for full-on utf-8 goodness.
alias larger='echo -en "\033]50;-misc-fixed-medium-r-normal--18-*-*-*-*-*-iso10646-1\007"'
alias huge='echo -en "\033]50;-misc-fixed-medium-r-normal--20-200-75-75-c-100-iso10646-1\007"'
alias normal=default
#if [ "$TERM" = "xterm" ] && [ "$LINES" -ge 26 ] && [ "$COLUMNS" -ge 100 ] && [ -z "$SSH_CONNECTION" ]; then
#	large
#fi

# Allow for use of =command to test for commands below, w/o error messages.
setopt nonomatch

alias df='df -h -x none'
alias free='free -m'
alias cls=clear
c () { cd; clear }
# Do we have GNU ls of a new enough version for color?
(ls --help 2>/dev/null |grep -- --color=) >/dev/null && \
        alias ls='ls -b -CF --color=auto'
alias l=ls
alias dir='ls -lha'
alias du='du -h'
alias cal='cal -3'
alias f=finger
alias edit=$EDITOR
alias e=$EDITOR
alias ftp=lftp
alias md=mkdir
# Put alive hosts at the bottom.
alias ruptime='ruptime -rt'
alias rd=rmdir
alias k=killall
alias mtr='mtr --curses'
[[ -x =reportbug ]] && alias bug='reportbug -b --no-check-available -x -M --mode=expert'
alias slrn='slrn -n -C-'
alias d=dict
alias menu=pdmenu
alias stardate='date "+%y%m.%d/%H%M"'
# Finger macros alias.
if [[ -e /proc/acpi ]]; then
	alias apm=acpi
fi
# I don't like the zsh builtin time command.
[[ -x =time ]] && alias time='command time'
alias bc='bc -l -q'
alias zxgv=xzgv
alias debcommit='debcommit -a'
alias youtube-dl='youtube-dl -t --max-quality=43'

# Stuff that I just don't want to run, because it sucks.
alias eview='echo "eview? No"'
# Argh, mf is so bloody annoying.
alias mf='echo "Er, if you want mv, type mv; if you want mf, I pity you."'

# Calculator
calc () { echo $* |bc -l }

# This fingers @host.
@ () { finger @$1 }

# Not for root.
if [[ "$USER" != root ]]; then
	alias cardctl='sudo cardctl'
	alias fetchnews='sudo fetchnews'
fi

# I don't like this for interactive use.
setopt nomatch

# Prompt {{{1

coreprompt="%n@%m"

# The extraprompt thing can be set when I have chroots and I want to keep
# track of which I am in.
if [ -e /etc/zsh.extraprompt ]; then
	baseprompt="$coreprompt(`cat /etc/zsh.extraprompt`):%~"
elif [ -e /etc/debian_chroot ]; then
	baseprompt="$coreprompt(`cat /etc/debian_chroot`):%~"
else
	baseprompt="$coreprompt:%~"
fi

# The prompt shows up in xterm titles too.
case $TERM in
   xterm|color_xterm|screen)
   	# Beware: next line has embedded raw control characters..
	PROMPT="%{]0;$baseprompt%}$baseprompt>"

	# Before each command is run, throw the command line up into the
	# titlebar! (The only problem is that very quick commands flicker..)
	preexec () {
		print -nP '\033]0;'
		# Truncate at 60 chars, escape %, escape invisibles
		print -nPR "%60>...>${(V)1//\%/%%}"
		print -nP " ($coreprompt)"
		print -n '\007'
		# Also set the screen window title to programs I run.
		if [ "$TERM" = screen ] && [ -z "$TMUX" ]; then
			print -nP '\033k%'
			print -nPR "%60>...>${(V)1//\%/%%}"
			print -n '\033\\'
		fi
	}
	
	# Add to the prompt to unset screen window title after program
	# exits.
	if [ "$TERM" = screen ]; then
		PROMPT="%{kzsh\\%}$PROMPT"
	fi

  ;;
  *) PROMPT="$baseprompt>"
  ;;
esac
unset baseprompt
[[ UID -eq 0 ]] && PROMPT=${PROMPT/\}%n/\}%U%n%u}

# Misc {{{1

limit core 0		# no core dumps
umask 022       	# no user groups any more
setopt nopromptcr	# No cr before prompt
#setopt extendedglob 	# Lots of cool extra wildcards
#setopt mailwarning	# New mail?
setopt correct		# Correct commands
# Note that the following comment makes me cringe now, but is left in for
# historical amusement value.
setopt autolist automenu nobeep	# Filename completion: the best of csh and 4dos!
setopt autocd		# Exec directory to cd there
setopt noclobber noflowcontrol
#setopt autoresume
setopt print_exit_value
setopt list_packed
unsetopt bgnice

# History setup
setopt share_history
setopt csh_junkie_history
setopt hist_ignore_dups
setopt hist_allow_clobber
setopt hist_reduce_blanks

# Force emacs editing mode.
[[ $TERM == "xterm" ]] && bindkey -e

# ctrl-s will no longer freeze the terminal.
#stty -ixon kill ^K
stty erase "^?"

eval `dircolors 2>/dev/null` # set up color-ls variables
watch=(notme)   # watch for everybody but me
LOGCHECK=120    # check every 2 min for login/logout activity

if [ -e ~/.security-environment ]; then
	printf "Current security environment: "
	cat ~/.security-environment
fi

# Turn on full-featured completion (needs 2.1) {{{2
if [[ "$ZSH_VERSION" == (3.1|4|5)* ]]; then
	autoload -U compinit
	compinit
fi
	
# Key bindings {{{1

bindkey '\e[A' history-search-backward
bindkey '\e[B' history-search-forward
bindkey '^K' kill-whole-line
bindkey -s '^L' "|less\n"		# ctrl-L pipes to less
bindkey -s '^B' " &\n"			# ctrl-B runs it in the background
bindkey "\e[1~" beginning-of-line	# Home (console)
bindkey "\e[H" beginning-of-line	# Home (xterm)
bindkey "\e[4~" end-of-line		# End (console)
bindkey "\e[F" end-of-line		# End (xterm)
bindkey "\e[2~" overwrite-mode		# Ins
bindkey "\e[3~" delete-char		# Delete
