fun! HaskellKeyword() abort
  let col = col('.')
  " Walk backward from the previous line to the first line of the buffer
  for l in range(line('.')-1, 1, -1)
    let byte = getline(l)[col - 1]
    if byte =~ '\k'
      " Reached a line above where the current column is a "word" character
      " Move the cursor to that position and return the word under the cursor
      call setpos('.', [0, l, col, 0])
      return expand('<cword>')
    elseif byte !~ '\s'
      " Found a line where the column is neither a word nor whitespace, bail out
      return ''
    endif
  endfor
  return ''
endfun

inoremap <expr> <C-r><C-w> HaskellKeyword()
