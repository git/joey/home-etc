. $HOME/.bashrc

# work around dircolors not supporting alacritty
# https://lists.gnu.org/archive/html/bug-coreutils/2019-05/msg00030.html
eval "$(TERM=xterm-256color dircolors --sh)"
export USER_LS_COLORS="$LS_COLORS"

# display screen session only on login
if [ "$TERM" != screen ] && command -v screen >/dev/null; then
	screen -q -ls
	if [ $? -ge 10 ]; then
		screen -ls
	fi
fi

