syntax on
set background=light

" Terminal setup.
set vb t_vb=""				" No beeps or visual bells.
if &term == "xterm"
	set t_kD=			" Make delete work in xterm.
	set t_kb=			" And backspace too.
endif

" How things look.
" Syntax colors.
highlight Todo ctermbg=Red ctermfg=Yellow
" Set up the status line so it's colored and always on. Very jedish.
set laststatus=2
highlight StatusLine cterm=none ctermbg=DarkBlue ctermfg=white
highlight StatusLineNC cterm=none ctermbg=DarkBlue ctermfg=white
highlight VertSplit cterm=none ctermbg=DarkBlue ctermfg=white

" Perl syntax highlighting controls.
let perl_want_scope_in_variables = 1
let perl_include_POD = 1
let perl_extended_vars = 1
"let perl_fold=1 " slow!!
let perl_nofold_packages=1

" Miscellaneous settings.
set pastetoggle=<C-P>	" I paste a lot, so make it easy.
set shortmess+=I	" Short messages for intro screen
set textwidth=75
set noicon		" I disable these because they can cause X traffic
set notitle		" bad over slow links. Plus my shell does it ok.
set clipboard=exclude:.* " ignore X clipboard, too slow remotely.
set nowrap		" don't display long lines wrapped, just scroll over
set backup		" make backups
set backupdir=~/tmp	" to tmp though, to cut down on the cruft
set showmatch		" highlight matching brackets
set showcmd		" show commands as they're being typed
set incsearch		" incremental searching
set lazyredraw		" don't redraw screen during macros and stuff
set autowrite		" write contents of file before eg, :make
set ignorecase		" ignore case while searching
set smartcase		" don't ignore uppercase characters I type tho
set wildmenu		" display menu of completions
set nomodeline
"set foldmethod=marker " DISABLED; breaks notmuch
set nofoldenable
set gcr=n:blinkon0	" no blinking cursor in gvim
set tags=.tags,tags	" use .tags for tags file; to reduce ls clutter
set nojoinspaces	" don't add spaces after period on gqip
set fileencodings=utf-8
"set termencoding=latin1
set viminfo='20,\"1000  " read/write a .viminfo file, with a larger than usual buffer
set autoindent		" indent next line same as current one
set noexpandtab         " don't replace tabs with spaces
"set undofile
"set undodir=~/tmp

" Enable it all.
filetype plugin indent on

" visible tab indent lines
"set list lcs=tab:\│\ 

" Abbreviations.
iab jjjj --[[Joey]]

" for my phone, which has no escape key
if hostname() == "limpet"
    imap @@ <Esc>
endif

" Autocommands.

" Transparent editing of gpg encrypted files.
" By Wouter Hanegraaff <wouter@blub.net>
augroup encrypted
    au!

    " First make sure nothing is written to ~/.viminfo while editing
    " an encrypted file.
    autocmd BufReadPre,FileReadPre      *.gpg set viminfo=
    " We don't want a swap file, as it writes unencrypted data to disk
    autocmd BufReadPre,FileReadPre      *.gpg set noswapfile
    " Switch to binary mode to read the encrypted file
    autocmd BufReadPre,FileReadPre      *.gpg set bin
    autocmd BufReadPre,FileReadPre      *.gpg let ch_save = &ch|set ch=2
    autocmd BufReadPost,FileReadPost    *.gpg '[,']!gpg --decrypt 2> /dev/null
    " Switch to normal mode for editing
    autocmd BufReadPost,FileReadPost    *.gpg set nobin
    autocmd BufReadPost,FileReadPost    *.gpg let &ch = ch_save|unlet ch_save
    autocmd BufReadPost,FileReadPost    *.gpg execute ":doautocmd BufReadPost " . expand("%:r")

    " Convert all text to encrypted text before writing
    autocmd BufWritePre,FileWritePre    *.gpg   '[,']!gpg --default-key=2512E3C7 --default-recipient-self -ae 2>/dev/null
    " Undo the encryption so we are back in the normal text, directly
    " after the file has been written. 
    autocmd BufWritePost,FileWritePost    *.gpg   u
augroup END

" Additional filetypes
autocmd! BufRead,BufNewFile *.wml	set filetype=html
autocmd! BufRead,BufNewFile *.t		set filetype=perl
autocmd! BufRead,BufNewFile *.otl.gpg	set filetype=vo_base
"autocmd! BufNewFile,BufRead *.mdwn	set filetype=ikiwiki

" Setup for outlining.
autocmd! BufRead,BufNewFile *.otl	set foldenable
autocmd! BufRead,BufNewFile *.otl.gpg	set foldenable
autocmd BufRead,BufNewFile *.otl	nmap <CR> za
autocmd BufRead,BufNewFile *.otl.gpg	nmap <CR> za
autocmd BufRead *.otl			:.,$foldclose
autocmd BufRead *.otl.gpg		:.,$foldclose

" Line length for mail
autocmd FileType mail			set textwidth=72

" avoid mini buf explorer getting larger than 1 line
let g:miniBufExplMaxSize = 1

let g:quickfixsigns_classes = ['qfl', 'vcsdiff', 'breakpoints']

" Ask hdevtools for the type of the expression under the cursor
" mnemic: eXamine
" (ctrl-x is usually some bit banging thing, not likely to be useful in haskell)
au FileType haskell nnoremap <buffer> <C-x> :HdevtoolsType<CR>
" XXX
let g:hdevtools_options = '-g -idist/build/git-annex/git-annex-tmp -g -Wall --socket=/home/joey/src/git-annex/.hdevtools.sock -g /home/joey/src/git-annex/dist/build/git-annex/git-annex-tmp/Utility/libdiskfree.o -g /home/joey/src/git-annex/dist/build/git-annex/git-annex-tmp/Utility/libmounts.o -g -XPackageImports'

" ctrl-Down saves the file and runs syntastic on it, in either mode
imap <C-Down> <esc>:w<CR>:SyntasticCheck<CR>a
noremap <C-Down> :w<CR>:SyntasticCheck<CR>
let g:syntastic_mode_map = { 'mode': 'passive' }
" open error display on error, close when none detected
let g:syntastic_auto_loc_list=1
let g:syntastic_loc_list_height=5
" wrap error display
autocmd bufreadpost quickfix setlocal wrap
" jump to errors
" let g:syntastic_auto_jump=1

" ctrl-Up to copy function from previous line (haskell)
inoremap <expr> <C-Up> HaskellKeyword()

" ctrl-Right and ctrl-Left following to a tag, and back, in either mode
set tagstack
imap <C-right> <esc><C-]>a
noremap <C-right> <C-]>
imap <C-left> <esc>:pop<CR>a
noremap <C-left> :pop<CR>

" shift-Right and shift-Left navigate through the quickfix list
imap <S-right> <esc>:cnext<CR>
noremap <S-right> :cnext<CR>
imap <S-left> <esc>:cprev<CR>
noremap <S-left> :cprev<CR>

" Allow dots in tag keywords. Also allow ' (for haskell)
set iskeyword=a-z,A-Z,_,.,39

" ctrl-b to :make
noremap <C-B> :make<CR>

" mappings for using a presentation clicker with vim-outliner
"noremap <PageDown> <Down>
"noremap <PageUp> <Up>
"map <Up> <Return>
"map <Down> <Return>
